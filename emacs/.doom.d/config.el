;; Doom config
(setq doom-theme 'doom-1337)

(setq display-line-numbers-type t)

(setq org-directory "~/org/")

;; Drag stuff config
(drag-stuff-global-mode 1)
(drag-stuff-define-keys)
